import itertools
import time

import numpy as np
import matplotlib.pyplot as plt
import random


class Particle:
    def __init__(self, coords):
        self.coords = coords
        self.velocity = [random.random()*2-1, random.random()*2-1]
        self.best = coords


def plot_particle(particle):
    plt.plot(particle.coords[0], particle.coords[1], marker="o", markersize=2, color="blue")


def fun(x, y):
    return (1.5 - x - x * y) ** 2 + (2.25 - x + x * y ** 2) ** 2 + (2.625 - x + x * y ** 3) ** 2


def generate_particles():
    particles = []
    for x in range(-45, 45, 5):
        for y in range(-45, 45, 5):
            particles.append(Particle([x / 10, y / 10]))
    return particles


def main():
    particles = generate_particles()
    plt.xlim(-4.5, 4.5)
    plt.ylim(-4.5, 4.5)
    plt.grid()

    plt.show(block=False)

    best_coords = None
    best_cost = 9999999999999999999
    for p in particles:
        cost = fun(p.coords[0], p.coords[1])
        if cost < best_cost:
            best_coords = p.coords
            best_cost = cost

    for i in range(200):
        plt.clf()
        plt.xlim(-4.5, 4.5)
        plt.ylim(-4.5, 4.5)
        plt.grid()
        new_best_coords = None
        new_best_cost = 9999999999999999999
        for p in particles:
            cost = fun(p.coords[0], p.coords[1])
            if cost < new_best_cost:
                new_best_coords = p.coords
                new_best_cost = cost

            p.velocity[0] += 0.05 * (best_coords[0] - p.coords[0])
            p.velocity[1] += 0.05 * (best_coords[1] - p.coords[1])

            p.velocity[0] += 0.05 * (p.best[0] - p.coords[0])
            p.velocity[1] += 0.05 * (p.best[1] - p.coords[1])

            p.velocity[0] *= 0.9
            p.velocity[1] *= 0.9

            p.coords[0] += 0.5*p.velocity[0]
            p.coords[1] += 0.5*p.velocity[1]

            plot_particle(p)
        best_cost = new_best_cost
        best_coords = new_best_coords
        plt.pause(0.01)

    plt.pause(1000000)


if __name__ == '__main__':
    main()
